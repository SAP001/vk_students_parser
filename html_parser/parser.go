package html_parser

import (
	"fmt"
	"golang.org/x/net/html"
	"log"
	"net/http"
)

func ParseNames() map[string][]string {
	shortToFullName := make(map[string][]string)
	resp, err := http.Get("https://kakzovut.ru/kakoe-polnoe-imya.html")
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	doc, err := html.Parse(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var f func(*html.Node)
	f = func(n *html.Node) {
		if n.Type == html.ElementNode && n.Data == "tr" {
			if n.FirstChild != nil && n.LastChild != nil &&
				n.FirstChild.Data == "td" && n.LastChild.Data == "td" {

				shortNameTag := n.FirstChild.FirstChild
				fullNameTags := n.LastChild

				if shortNameTag.Type == html.TextNode && shortNameTag.Data != "Краткое" {
					var fullNames []string

					firstA := fullNameTags.FirstChild
					for c := firstA; c != nil; c = c.NextSibling {

						if c.FirstChild != nil {
							fullNames = append(fullNames, c.FirstChild.Data)
						}
					}
					shortToFullName[shortNameTag.Data] = append(shortToFullName[shortNameTag.Data], fullNames...)
				}
			}
		}
		for c := n.FirstChild; c != nil; c = c.NextSibling {
			f(c)
		}
	}
	f(doc)
	return shortToFullName
}
