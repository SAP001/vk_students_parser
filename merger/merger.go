package merger

import (
	"encoding/json"
	"fmt"
	"gitlab.com/vk_students_parser/fill_users"
	"io/ioutil"
	"net/http"
	"strconv"
)

func FirstMerge(realStudents []Student, VKPossibleStudents []fill_users.UserInfo, shortNameToFull map[string][]string) []fill_users.UserInfo {
	lastNameToVKPossibleStudent := getLastNameToVVPossibleStudent(VKPossibleStudents)

	for i, realStudent := range realStudents {
		lastNameMatches := lastNameToVKPossibleStudent[realStudent.LastName]
		if lastNameMatches == nil {
			continue
		}

		fullNameMatches := searchFullNameMatches(realStudent, lastNameMatches, shortNameToFull)
		if len(fullNameMatches) == 1 {
			realStudents[i].VkID = fullNameMatches[0].Id
		}
	}

	return VKPossibleStudents
}

func Merge(realStudents []Student, VKPossibleStudents []fill_users.UserInfo, shortNameToFull map[string][]string, token string) []Student {
	lastNameToVKPossibleStudent := getLastNameToVVPossibleStudent(VKPossibleStudents)
	groupToStudent := getGroupToStudent(realStudents)

	for i, realStudent := range realStudents {
		if realStudent.VkID != 0 {
			continue
		}
		lastNameMatches := lastNameToVKPossibleStudent[realStudent.LastName]
		if lastNameMatches == nil {
			continue
		}

		fullNameMatches := searchFullNameMatches(realStudent, lastNameMatches, shortNameToFull)
		if len(fullNameMatches) == 1 {
			realStudents[i].VkID = fullNameMatches[0].Id
		} else if len(fullNameMatches) > 1 {
			realStudents[i].VkID = chooseRealStudentID(realStudent.Group, fullNameMatches, groupToStudent, token)
		}
	}

	return realStudents
}

func chooseRealStudentID(realStudentGroup string, fullNameMatches []fill_users.UserInfo, groupToStudent map[string][]Student, token string) int {
	classmates := groupToStudent[realStudentGroup]
	// Вытащить id одногруппников
	classMatesIDs := getClassMatesIDs(classmates)
	// Собрать id друзей одногруппников
	classMateFriendIDs := getClassMateFriendIDs(classMatesIDs, token)

	// Считаем включения в друзья
	possibleStudentIdToCountMatch := make(map[int]int)
	for _, possibleStud := range fullNameMatches {
		possibleStudentIdToCountMatch[possibleStud.Id] = countMatch(possibleStud.Id, classMateFriendIDs)
	}

	//Выбираем наиболее вероятного
	var bestPossibleStudentID int
	var bestPossibleStudentMatches int
	for key, val := range possibleStudentIdToCountMatch {
		if val > bestPossibleStudentMatches {
			bestPossibleStudentMatches = val
			bestPossibleStudentID = key
		}
	}
	if bestPossibleStudentMatches > 0 {
		return bestPossibleStudentID
	}
	return 0
}

func countMatch(id int, ids []int) int {
	var counter int
	for _, val := range ids {
		if val == id {
			counter++
		}
	}
	return counter
}

type FriendsResp struct {
	Response struct {
		Count int   `json:"count"`
		Items []int `json:"items"`
	} `json:"response"`
}

func getClassMateFriendIDs(classMatesIDs []int, token string) []int {
	var ids []int

	for _, id := range classMatesIDs {
		resp, err := http.Get("https://api.vk.com/method/friends.get?lang=ru&user_id=" + strconv.Itoa(id) + "&access_token=" + token + "&v=5.131")
		if err != nil {
			fmt.Println(err)
		}
		defer resp.Body.Close()
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			fmt.Println(err)
		}

		var tmp FriendsResp
		err = json.Unmarshal(respBody, &tmp)
		if err != nil {
			fmt.Println("res Body", err)
		}

		ids = append(ids, tmp.Response.Items...)
	}
	return ids
}

func getClassMatesIDs(classmates []Student) []int {
	var ids []int
	for _, c := range classmates {
		if c.VkID != 0 {
			ids = append(ids, c.VkID)
		}
	}
	return ids
}

func searchFullNameMatches(realStudent Student, lastNameMatches []fill_users.UserInfo, shortNameToFull map[string][]string) []fill_users.UserInfo {
	var fullNameMatches []fill_users.UserInfo
	for _, possibleName := range lastNameMatches {
		//Метчинг по имени "как есть"
		if possibleName.FirstName == realStudent.FirstName {
			fullNameMatches = append(fullNameMatches, possibleName)
			continue
		}

		// Ппытка привести к полному имени
		fullNames := shortNameToFull[possibleName.FirstName]
		for _, name := range fullNames {
			if name == realStudent.FirstName {
				fullNameMatches = append(fullNameMatches, possibleName)
				continue
			}
		}
	}
	return fullNameMatches
}

func getGroupToStudent(users []Student) map[string][]Student {
	groupToStudent := make(map[string][]Student)
	for _, user := range users {
		groupToStudent[user.Group] = append(groupToStudent[user.Group], user)
	}
	return groupToStudent
}

func getLastNameToVVPossibleStudent(users []fill_users.UserInfo) map[string][]fill_users.UserInfo {
	lastNameToVKPossibleStudent := make(map[string][]fill_users.UserInfo)
	for _, user := range users {
		lastNameToVKPossibleStudent[user.LastName] = append(lastNameToVKPossibleStudent[user.LastName], user)
	}
	return lastNameToVKPossibleStudent
}
