package merger

type Student struct {
	FirstName   string `json:"first_name,omitempty"`
	LastName    string `json:"last_name,omitempty"`
	Patronymic  string `json:"patronymic,omitempty"`
	RegisterNum string `json:"register_num,omitempty"`
	VkID        int    `json:"vk_id,omitempty"`
	VkProfile   string `json:"vk_profile,omitempty"`
	DateOfBirth string `json:"date_of_birth"`
	Group       string `json:"group"`
	Specialty   string `json:"specialty,omitempty"`
}
