package fill_users

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"sync"
)

type UserInfo struct {
	Id        int    `json:"id"`
	FirstName string `json:"first_name"`
	LastName  string `json:"last_name"`
}

type UserInfoResp struct {
	Response []UserInfo `json:"response"`
}

func SearchFriends(users []int, tokens []string) []UserInfo {
	usersFull := make([][]UserInfo, len(tokens))
	var result []UserInfo

	batches := make([]string, len(users)/1000+1)
	for i, userID := range users {
		batches[i/1000] += strconv.Itoa(userID) + ","
	}
	for i := range batches {
		batches[i] = batches[i][:len(batches[i])-1]
	}

	wg := sync.WaitGroup{}
	for i, t := range tokens {
		oneBatchSize := len(batches) / len(tokens)
		firstBatchIndex := oneBatchSize * i
		lastBatchIndex := oneBatchSize * (i + 1)

		wg.Add(1)
		go func(wg *sync.WaitGroup, gorutineID int, batches []string, t string) {
			defer wg.Done()
			for i := range batches {
				resp, err := http.Get("https://api.vk.com/method/users.get?lang=ru&user_ids=" + batches[i] + "&access_token=" + t + "&v=5.131")
				if err != nil {
					fmt.Println(err)
				}

				defer resp.Body.Close()
				respBody, err := ioutil.ReadAll(resp.Body)
				if err != nil {
					log.Fatal(err)
				}

				var tmp UserInfoResp
				err = json.Unmarshal(respBody, &tmp)
				if err != nil {
					fmt.Println("res Body", err)
				}
				usersFull[gorutineID] = append(usersFull[gorutineID], tmp.Response...)

			}
		}(&wg, i, batches[firstBatchIndex:lastBatchIndex], t)
	}
	wg.Wait()

	for _, r := range usersFull {
		result = append(result, r...)
	}
	return result
}
