package formatter

import (
	"encoding/json"
	"gitlab.com/vk_students_parser/fill_users"
	"gitlab.com/vk_students_parser/merger"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strings"
)

func Format(realStudents []merger.Student, VKPossibleStudents []fill_users.UserInfo) ([]merger.Student, []fill_users.UserInfo) {
	VKPossibleStudents = translate(VKPossibleStudents)
	for i := range VKPossibleStudents {
		VKPossibleStudents[i].FirstName = strings.Replace(VKPossibleStudents[i].FirstName, "ё", "е", -1)
		VKPossibleStudents[i].LastName = strings.Replace(VKPossibleStudents[i].LastName, "ё", "е", -1)
	}
	for i := range realStudents {
		realStudents[i].FirstName = strings.Replace(realStudents[i].FirstName, "ё", "е", -1)
		realStudents[i].LastName = strings.Replace(realStudents[i].LastName, "ё", "е", -1)
	}
	return realStudents, VKPossibleStudents
}

func translate(users []fill_users.UserInfo) []fill_users.UserInfo {
	for i := range users {
		users[i].FirstName = translateWord(users[i].FirstName)
		users[i].LastName = translateWord(users[i].LastName)
	}
	return users
}

var IsEng = regexp.MustCompile(`[a-zA-Z].`).MatchString

type translateResp struct {
	Translate string `json:"translate"`
}

func translateWord(enWord string) string {
	if !IsEng(enWord) || enWord == "DELETED" {
		return enWord
	}
	reg := regexp.MustCompile(`[a-zA-Z]+`)
	enWord = reg.FindString(enWord)
	if enWord == "" {
		return enWord
	}

	resp, err := http.Get("http://127.0.0.1:8080/?word=" + enWord)
	if err != nil {
		log.Fatal(err)
	}

	defer resp.Body.Close()
	respBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		log.Fatal(err)
	}

	var tmp translateResp
	err = json.Unmarshal(respBody, &tmp)
	if err != nil {
		return enWord
	}
	return tmp.Translate
}
