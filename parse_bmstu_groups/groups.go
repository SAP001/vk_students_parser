package parse_bmstu_groups

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

type GroupItem struct {
	Id   int    `json:"id"`
	Name string `json:"name"`
}

type Groups struct {
	Response struct {
		Count int         `json:"count"`
		Items []GroupItem `json:"items"`
	} `json:"response"`
}

var stopWords = []string{"станкин", "станкин", "носова", "носов", "Кафе"}
var groupNameInclude = []string{"мгту", "bmstu", "BAUMAN", "баумана", "Баумана Мытищи", "КФ МГТУ", "Баумана(Калуга)"}

func FindBmstuGroupsID(token string) []GroupItem {
	groups := make(map[int]GroupItem)

	for _, name := range groupNameInclude {
		url := "https://api.vk.com/method/groups.search?count=100&q=" + name + "&access_token=" + token + "&v=5.131"
		resp, err := http.Get(url)
		if err != nil {
			fmt.Println(err)
		}

		defer resp.Body.Close()
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		tmp := Groups{}
		err = json.Unmarshal(respBody, &tmp)
		if err != nil {
			fmt.Println("res Body", err)
		}

		for _, g := range tmp.Response.Items {
			if UnIncludeStopWords(g.Name) && IncludeWords(g.Name) {
				groups[g.Id] = g
			}
		}
	}

	var group []GroupItem
	for _, g := range groups {
		group = append(group, g)
	}
	return group
}

func UnIncludeStopWords(str string) bool {
	for _, stopWord := range stopWords {
		if strings.Contains(strings.ToLower(str), strings.ToLower(stopWord)) {
			return false
		}
	}
	return true
}
func IncludeWords(str string) bool {
	for _, stopWord := range groupNameInclude {
		if strings.Contains(strings.ToLower(str), strings.ToLower(stopWord)) {
			return true
		}
	}
	return false
}

//var groups []parse_bmstu_groups.GroupItem
//jsonFile, err := os.Open("./source/groups.json")
//if err != nil {
//fmt.Println(err)
//}
//j, _ := ioutil.ReadAll(jsonFile)
//json.Unmarshal(j, &groups)
//
//posStudent := parse_possible_students.FindBmstuPossibleStudentsID(groups, token)
//
//b, err := json.MarshalIndent(posStudent, "", "")
//if err != nil {
//log.Fatal("failed to marshal matriculants")
//}
//
//err = ioutil.WriteFile("./source/vk_possible_students.json", b, 0644)
//if err != nil {
//log.Fatal("failed to save json: %w ", err)
//}
