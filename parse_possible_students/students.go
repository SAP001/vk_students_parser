package parse_possible_students

import (
	"encoding/json"
	"gitlab.com/vk_students_parser/parse_bmstu_groups"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
	"time"
)

type Users struct {
	Response struct {
		Count int   `json:"count"`
		Items []int `json:"items"`
	} `json:"response"`
}

func FindBmstuPossibleStudentsID(groups []parse_bmstu_groups.GroupItem, token string) []int {
	possibleStudents := make(map[int]struct{})
	var requestCounter int

	for _, g := range groups {
		resp, err := http.Get("https://api.vk.com/method/groups.getMembers?count=1000&group_id=" + strconv.Itoa(g.Id) + "&access_token=" + token + "&v=5.131")
		if requestCounter%20 == 0 {
			time.Sleep(time.Second * 2)
		}
		requestCounter++
		if err != nil {
			log.Fatal(err)
		}
		defer resp.Body.Close()
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		var tmp Users
		err = json.Unmarshal(respBody, &tmp)
		if err != nil {
			log.Fatal("res Body", err)
		}

		for _, uID := range tmp.Response.Items {
			possibleStudents[uID] = struct{}{}
		}

		for i := 1000; i < tmp.Response.Count; i += 1000 {
			if requestCounter%20 == 0 {
				time.Sleep(time.Second * 2)
			}
			resp, err = http.Get("https://api.vk.com/method/groups.getMembers?count=1000&offset=" + strconv.Itoa(i) + "&group_id=" + strconv.Itoa(g.Id) + "&access_token=" + token + "&v=5.131")
			requestCounter++
			if err != nil {
				log.Fatal(err)
			}
			defer resp.Body.Close()
			respBody, err = ioutil.ReadAll(resp.Body)
			if err != nil {
				log.Fatal(err)
			}

			err = json.Unmarshal(respBody, &tmp)
			if err != nil {
				log.Fatal("res Body", err)
			}

			for _, uID := range tmp.Response.Items {
				possibleStudents[uID] = struct{}{}
			}
		}
	}

	var uID []int
	for sID := range possibleStudents {
		uID = append(uID, sID)
	}
	return uID
}
