package add_vk_possible_students_friends

import (
	"encoding/json"
	"fmt"
	"gitlab.com/vk_students_parser/parse_possible_students"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"
)

func FillUsers(users []int, token string) []int {
	usersWithFriendsUiniq := make(map[int]struct{})
	var usersWithFriends []int

	for _, userID := range users {
		resp, err := http.Get("https://api.vk.com/method/friends.get?user_id=" + strconv.Itoa(userID) + "&access_token=" + token + "&v=5.131")
		if err != nil {
			fmt.Println(err)
		}

		defer resp.Body.Close()
		respBody, err := ioutil.ReadAll(resp.Body)
		if err != nil {
			log.Fatal(err)
		}

		tmp := parse_possible_students.Users{}
		err = json.Unmarshal(respBody, &tmp)
		if err != nil {
			fmt.Println("res Body", err)
		}

		usersWithFriendsUiniq[userID] = struct{}{}
		for _, fID := range tmp.Response.Items {
			usersWithFriendsUiniq[fID] = struct{}{}
		}
	}

	for userID := range usersWithFriendsUiniq {
		usersWithFriends = append(usersWithFriends, userID)
	}
	return usersWithFriends
}
